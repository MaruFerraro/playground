package com.playground.apiinscription;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiInscriptionApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiInscriptionApplication.class, args);
	}

}
