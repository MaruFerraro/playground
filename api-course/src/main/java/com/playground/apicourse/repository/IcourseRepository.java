package com.playground.apicourse.repository;

import com.playground.apicourse.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IcourseRepository extends JpaRepository<Course, Long> {
}
