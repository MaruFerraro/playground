package com.playground.apicourse.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.*;


@Entity
@Table(name = "courses")
@Getter
@Setter
@AllArgsConstructor
public class Course {

    @Id
    @Column(name = "course_id", unique = true, nullable = false)
    private Long courseId;
    @Column(name = "name", unique = true, nullable = false)
    private String name;
}
