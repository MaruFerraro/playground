package com.playground.apistudent.service;


import com.playground.apistudent.exceptions.StudentException;
import com.playground.apistudent.model.Student;

import java.util.List;

public interface IStudentService {

    Long create(Student student);
    Student getById(Long studentId) throws StudentException;
    List<Student> getAll();
    Long deleteById(Long studentId);
    Long update(Student student);
}
