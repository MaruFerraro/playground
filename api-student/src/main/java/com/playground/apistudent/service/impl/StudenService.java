package com.playground.apistudent.service.impl;

import com.playground.apistudent.exceptions.MessageError;
import com.playground.apistudent.exceptions.StudentException;
import com.playground.apistudent.model.Student;
import com.playground.apistudent.repository.IStudentRepository;
import com.playground.apistudent.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudenService implements IStudentService {

    @Autowired
    private IStudentRepository studentRepository;

    @Override
    public Long create(Student student) {
        studentRepository.save(student);
        return null;
    }

    @Override
    public Student getById(Long studentId) throws StudentException {
        return studentRepository.findById(studentId).orElseThrow(() -> new StudentException(MessageError.STUDENT_DOES_NOT_EXIST));
    }

    @Override
    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    @Override
    public Long deleteById(Long studentId) {
        studentRepository.deleteById(studentId);
        return studentId;
    }

    @Override
    public Long update(Student student) {
        return null;
    }
}
